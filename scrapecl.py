# import libs
from pyquery import PyQuery as pq
import os, re, datetime, webbrowser, calendar, time, urlparse, urllib, argparse

# define CLPost obj
class CLPost(object):
    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

# get var from cli
parser = argparse.ArgumentParser(
    description='Scrapes CL'
)

parser.add_argument('-k', '--keyword', metavar='keyword', required=False, help='Keyword to search for')
parser.add_argument('-e', '--endpoint', metavar='endpoint', required=False, help='Endpoint to search in')

args = parser.parse_args()

# domains
protocol = 'https://'
domains = ['sfbay.craigslist.org',
						'losangeles.craigslist.org',
						'newyork.craigslist.org',
						'seattle.craigslist.org',
						'chicago.craigslist.org',
						'orangecounty.craigslist.org',
						'portland.craigslist.org',
						'washingtondc.craigslist.org',
						'sandiego.craigslist.org',
						'boston.craigslist.org',
						'denver.craigslist.org',
						'sacramento.craigslist.org',
						'dallas.craigslist.org',
						'minneapolis.craigslist.org',
						'phoenix.craigslist.org',
						'atlanta.craigslist.org',
						'inlandempire.craigslist.org',
						'austin.craigslist.org',
						'miami.craigslist.org',
						'philadelphia.craigslist.org',
						'houston.craigslist.org',
						'newjersey.craigslist.org',
						'detroit.craigslist.org',
						'tampa.craigslist.org',
						'orlando.craigslist.org',
						'raleigh.craigslist.org',
						'lasvegas.craigslist.org',
						'cnj.craigslist.org',
						'baltimore.craigslist.org',
						'charlotte.craigslist.org',
						'milwaukee.craigslist.org',
						'nashville.craigslist.org',
						'kansascity.craigslist.org',
						'columbus.craigslist.org',
						'nh.craigslist.org',
						'stlouis.craigslist.org',
						'ventura.craigslist.org',
						'honolulu.craigslist.org',
						'sanantonio.craigslist.org',
						'cincinnati.craigslist.org',
						'richmond.craigslist.org',
						'hudsonvalley.craigslist.org',
						'indianapolis.craigslist.org',
						'hartford.craigslist.org',
						'providence.craigslist.org',
						'pittsburgh.craigslist.org',
						'maine.craigslist.org',
						'madison.craigslist.org',
						'boulder.craigslist.org',
						'longisland.craigslist.org',
						'cleveland.craigslist.org',
						'tucson.craigslist.org',
						'southjersey.craigslist.org',
						'grandrapids.craigslist.org',
						'bellingham.craigslist.org',
						'cosprings.craigslist.org',
						'worcester.craigslist.org',
						'spokane.craigslist.org',
						'santabarbara.craigslist.org',
						'newhaven.craigslist.org',
						'greenville.craigslist.org',
						'albany.craigslist.org',
						'eugene.craigslist.org',
						'boise.craigslist.org',
						'reno.craigslist.org',
						'fortcollins.craigslist.org',
						'asheville.craigslist.org',
						'annarbor.craigslist.org',
						'vermont.craigslist.org',
						'sarasota.craigslist.org',
						'fortmyers.craigslist.org',
						'westernmass.craigslist.org',
						'knoxville.craigslist.org',
						'oklahomacity.craigslist.org',
						'louisville.craigslist.org',
						'jacksonville.craigslist.org',
						'fresno.craigslist.org',
						'stockton.craigslist.org',
						'slo.craigslist.org',
						'modesto.craigslist.org',
						'greensboro.craigslist.org',
						'rochester.craigslist.org',
						'palmsprings.craigslist.org',
						'buffalo.craigslist.org',
						'norfolk.craigslist.org',
						'bakersfield.craigslist.org',
						'jerseyshore.craigslist.org',
						'dayton.craigslist.org',
						'charleston.craigslist.org',
						'akroncanton.craigslist.org',
						'tulsa.craigslist.org',
						'monterey.craigslist.org',
						'albuquerque.craigslist.org',
						'syracuse.craigslist.org',
						'lancaster.craigslist.org',
						'neworleans.craigslist.org',
						'omaha.craigslist.org',
						'newlondon.craigslist.org',
						'allentown.craigslist.org',
						'salem.craigslist.org',
						'springfield.craigslist.org',
						'desmoines.craigslist.org',
						'delaware.craigslist.org',
						'chico.craigslist.org',
						'columbiamo.craigslist.org',
						'columbia.craigslist.org',
						'bend.craigslist.org',
						'winstonsalem.craigslist.org',
						'harrisburg.craigslist.org',
						'daytona.craigslist.org',
						'chattanooga.craigslist.org',
						'gainesville.craigslist.org',
						'appleton.craigslist.org',
						'southbend.craigslist.org',
						'redding.craigslist.org',
						'ocala.craigslist.org',
						'lakeland.craigslist.org',
						'fredericksburg.craigslist.org',
						'corvallis.craigslist.org',
						'bham.craigslist.org',
						'anchorage.craigslist.org',
						'spacecoast.craigslist.org',
						'saltlakecity.craigslist.org',
						'rockford.craigslist.org',
						'fayar.craigslist.org',
						'lexington.craigslist.org',
						'lansing.craigslist.org',
						'huntsville.craigslist.org',
						'charlottesville.craigslist.org',
						'toledo.craigslist.org',
						'memphis.craigslist.org',
						'medford.craigslist.org',
						'goldcountry.craigslist.org',
						'athensga.craigslist.org',
						'annapolis.craigslist.org',
						'york.craigslist.org',
						'stcloud.craigslist.org',
						'greenbay.craigslist.org',
						'eauclaire.craigslist.org',
						'kalamazoo.craigslist.org',
						'eastnc.craigslist.org',
						'chambana.craigslist.org',
						'tallahassee.craigslist.org',
						'lincoln.craigslist.org',
						'fortwayne.craigslist.org',
						'easttexas.craigslist.org',
						'bozeman.craigslist.org',
						'wichita.craigslist.org',
						'tricities.craigslist.org',
						'treasure.craigslist.org',
						'southcoast.craigslist.org',
						'savannah.craigslist.org',
						'nwct.craigslist.org',
						'missoula.craigslist.org',
						'fayetteville.craigslist.org',
						'yakima.craigslist.org',
						'rmn.craigslist.org',
						'reading.craigslist.org',
						'pensacola.craigslist.org',
						'hickory.craigslist.org',
						'wilmington.craigslist.org',
						'merced.craigslist.org',
						'littlerock.craigslist.org',
						'kpr.craigslist.org',
						'killeen.craigslist.org',
						'frederick.craigslist.org',
						'easternshore.craigslist.org',
						'duluth.craigslist.org',
						'capecod.craigslist.org',
						'skagit.craigslist.org',
						'racine.craigslist.org',
						'flagstaff.craigslist.org',
						'collegestation.craigslist.org',
						'batonrouge.craigslist.org',
						'rockies.craigslist.org',
						'roanoke.craigslist.org',
						'myrtlebeach.craigslist.org',
						'mobile.craigslist.org',
						'iowacity.craigslist.org',
						'centralmich.craigslist.org',
						'cedarrapids.craigslist.org',
						'bloomington.craigslist.org',
						'wenatchee.craigslist.org',
						'wausau.craigslist.org',
						'smd.craigslist.org',
						'nwga.craigslist.org',
						'nmi.craigslist.org',
						'mcallen.craigslist.org',
						'flint.craigslist.org',
						'elpaso.craigslist.org',
						'binghamton.craigslist.org',
						'youngstown.craigslist.org',
						'scranton.craigslist.org',
						'quadcities.craigslist.org',
						'prescott.craigslist.org',
						'muskegon.craigslist.org',
						'mankato.craigslist.org',
						'macon.craigslist.org',
						'fargo.craigslist.org',
						'altoona.craigslist.org',
						'waco.craigslist.org',
						'tippecanoe.craigslist.org',
						'santamaria.craigslist.org',
						'pullman.craigslist.org',
						'lynchburg.craigslist.org',
						'lacrosse.craigslist.org',
						'joplin.craigslist.org',
						'ithaca.craigslist.org',
						'humboldt.craigslist.org',
						'dubuque.craigslist.org',
						'augusta.craigslist.org',
						'westslope.craigslist.org',
						'swmi.craigslist.org',
						'siouxcity.craigslist.org',
						'sanmarcos.craigslist.org',
						'oregoncoast.craigslist.org',
						'olympic.craigslist.org',
						'limaohio.craigslist.org',
						'hattiesburg.craigslist.org',
						'yuma.craigslist.org',
						'waterloo.craigslist.org',
						'staugustine.craigslist.org',
						'siouxfalls.craigslist.org',
						'santafe.craigslist.org',
						'poconos.craigslist.org',
						'panamacity.craigslist.org',
						'muncie.craigslist.org',
						'holland.craigslist.org',
						'fortsmith.craigslist.org',
						'erie.craigslist.org',
						'eastidaho.craigslist.org',
						'carbondale.craigslist.org',
						'ames.craigslist.org',
						'topeka.craigslist.org',
						'springfieldil.craigslist.org',
						'sheboygan.craigslist.org',
						'semo.craigslist.org',
						'pueblo.craigslist.org',
						'peoria.craigslist.org',
						'pennstate.craigslist.org',
						'northernwi.craigslist.org',
						'lawrence.craigslist.org',
						'janesville.craigslist.org',
						'evansville.craigslist.org',
						'dothan.craigslist.org',
						'clarksville.craigslist.org',
						'bn.craigslist.org',
						'blacksburg.craigslist.org',
						'stgeorge.craigslist.org',
						'saginaw.craigslist.org',
						'loz.craigslist.org',
						'jackson.craigslist.org',
						'gulfport.craigslist.org',
						'auburn.craigslist.org',
						'winchester.craigslist.org',
						'valdosta.craigslist.org',
						'utica.craigslist.org',
						'okaloosa.craigslist.org',
						'northmiss.craigslist.org',
						'lubbock.craigslist.org',
						'lafayette.craigslist.org',
						'ksu.craigslist.org',
						'keys.craigslist.org',
						'kalispell.craigslist.org',
						'harrisonburg.craigslist.org',
						'glensfalls.craigslist.org',
						'corpuschristi.craigslist.org',
						'chautauqua.craigslist.org',
						'wyoming.craigslist.org',
						'watertown.craigslist.org',
						'texoma.craigslist.org',
						'roseburg.craigslist.org',
						'provo.craigslist.org',
						'morgantown.craigslist.org',
						'mohave.craigslist.org',
						'klamath.craigslist.org',
						'huntington.craigslist.org',
						'elmira.craigslist.org',
						'billings.craigslist.org',
						'up.craigslist.org',
						'shreveport.craigslist.org',
						'lascruces.craigslist.org',
						'jacksontn.craigslist.org',
						'dublin.craigslist.org',
						'dubai.craigslist.org',
						'brownsville.craigslist.org',
						'fairbanks.craigslist.org',
						'cairo.craigslist.org',
						'amsterdam.craigslist.org',
						'amarillo.craigslist.org',
						'plattsburgh.craigslist.org',
						'costarica.craigslist.org',
						'puertorico.craigslist.org',
						'malaysia.craigslist.org',
						'auckland.craigslist.org']
# init posts list
posts = []

# loop through the domains
for domain in domains:
	
	#############################
	# this is the part you edit #
	#############################
	
	# change jjj to ggg to search for gigs instead of jobs
	url = protocol + domain + '/search/' + args.endpoint + '?'  + urllib.urlencode({
		# the keyword to search for. eg: wordpress, project manager, audio engineer etc.
		"query" : args.keyword,
		# leave this alone
		"sort" : "date",
		# comment this out if you don't want to search titles
		# "srchType" : "T",
		# when uncommented this will only surface ads that have telecommuting set to true
		# "is_telecommuting" : "1"
	})

	###################################
	# dont worry about the shit below #
	###################################


	# show the url we're scraping in terminal
	print(url)

	fp = None
	# test grab html page using urllib
	try:
		fp = urllib.urlopen(url)
	except Exception as e:
		continue
	html = fp.read().decode("utf8")
	fp.close()

	# parse html of page with PyQuery
	d = pq(html)
	# loop through results
	for row in d('.result-row').items():
		# year, month, day, hours, minutes
		date = list(map(lambda str: int(str), re.split("-|:|\s", row('.result-date').attr('datetime'))))

		# build CLPost obj
		cl = CLPost({
			"id" : row.attr('data-pid'),
			"title" : row(".result-title").text(),
			"link" : row(".result-title").attr('href'),
			"date" : datetime.datetime(date[0], date[1], date[2], date[3], date[4]),
			"domain" : '{uri.netloc}'.format(uri=urlparse.urlparse(row(".result-title").attr('href'))),
		})
		# add to list of posts
		posts.append(cl)
# dedupe
seen_titles = set()
no_dupes = []
for obj in posts:
    if obj.title not in seen_titles:
        no_dupes.append(obj)
        seen_titles.add(obj.title)

# sort by date desc
no_dupes.sort(key = lambda post: post.date, reverse = True)

# start output of file
filename = 'scrapecl-%s.html' % calendar.timegm(time.gmtime())
file = open(filename, "w")
file.write("""
	<html>
	<head>
		<style type="text/css">
			.posts{
				text-align: center;
				font-family: helvetica, verdana, sans-serif;
			}
			.posts-post-title{
				margin-right: 10px;
			}
			.posts-post-domain{
				margin-right: 10px;
			}
		</style>
	</head>
	<body>
		<div class="posts">
			<h1 class="posts-title">ScrapeCL</h1>
""")
# build post html from posts
for post in no_dupes:
	file.write('<div class="posts-post"><span class="posts-post-domain">%s</span><a target="_blank" href="%s" class="posts-post-title">%s</a><span class="posts-post-date">%s</span></div>' % (post.domain, post.link.encode('utf-8').strip(), post.title.encode('utf-8').strip(), post.date.strftime("%m/%d/%Y, %H:%M:%S")))
# close html
file.write('</div></body></html>')
# stop writing to html file
file.close()

# open in web browser
if os.name == 'posix':
	webbrowser.open_new_tab('file://%s' % os.path.abspath(filename))
else:
	webbrowser.open_new_tab(filename)